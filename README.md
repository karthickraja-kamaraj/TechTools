# Tech Tools 


## Scripts 


### SSH 

  A wrapper for the SSH to maintain a typescript for all the SSH connections we made on the machine for furture reference. 
  
### VIM 

  A wrapper to VIM command with a customized changes. 


### share 

  This is a script to share a document or directory with limited number of accesiblity over the network. 
  > written by "Simon Budig" named "whoof". 

### timestamp 
  simple date command substitution to use in CLI while backing up some files. 

### todo.wrapper.sh 
  a wrapper to todo.txt cli. 
  For maintaining the various TODO directory, this wrapper written to access all using a single command. 

#### Options 

    -l for to consider current directory as TODO home directory. 
    -d for today's todo files. 
    -w for current weeks todo files. 