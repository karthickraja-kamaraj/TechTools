#!/bin/bash 

localize=0; 
periodical=0; 
todo_sh='/home/karthickraja/Daybook/script/Tasks/todo.sh' 
dummy_cfg='/home/karthickraja/Daybook/script/Tasks/dummy.cfg' 
export TODO_ACTIONS_DIR="$HOME/Daybook/script/Tasks/.todo.actions.d/"
echo $PWD | egrep -q "Training|Projects" 
if [ $? -eq 1 ] ; then # not found 
        curdir=` echo $PWD | sed -r 's#^/[^/]+/[^/]+/([^/]+).*#\1#'`
else            
        curdir=` echo $PWD | sed -r 's#^/[^/]+/[^/]+/[^/]+/([^/]+).*#\1#'`
fi
if [[ $# -ge 1 && $1 == "-l" ]] ; then 
	localize=1; 
#	echo locallize 
	export TODO_DIR="$HOME/$curdir/Tasks"
	# Your todo/done/report.txt locations
	export TODO_FILE="$TODO_DIR/task.todo.txt"
	export DONE_FILE="$TODO_DIR/task.done.txt"
	export REPORT_FILE="$TODO_DIR/task.report.txt"

	# You can customize your actions directory location
	shift;
elif [[ $# -ge 1 && $1 == "-w" ]] ; then 
	if [[ $2 =~ txt$ ]] ; then 	# Invoked using the xox command used for navigation functionality. 
		periodical=1; 
		export TODO_DIR=`dirname $2` 
		# Your todo/done/report.txt locations
		export TODO_FILE=$2
		export DONE_FILE=$3 
		export REPORT_FILE=$4
		shift;shift;shift;shift;

	else
		exec xox -w $@ 
	fi 
elif [[ $# -ge 1 && $1 == "-d" ]] ; then 
	if [[ $2 =~ txt$ ]] ; then 	# Invoked using the xox command used for navigation functionality. 
		periodical=1; 
		export TODO_DIR=`dirname $2` 
		# Your todo/done/report.txt locations
		export TODO_FILE=$2
		export DONE_FILE=$3 
		export REPORT_FILE=$4
		shift;shift;shift;shift;

	else
		exec xox -d $@ 
	fi 
elif [[ $# -ge 1 && $1 == "-p" ]] ; then 
		periodical=1; 
		export TODO_DIR=`dirname $2.todo.txt` 
		# Your todo/done/report.txt locations
		export TODO_FILE=$2.todo.txt 
		export DONE_FILE=$3.done.txt 
		export REPORT_FILE=$4.report.txt 
		shift;shift;shift;shift;

fi 

if [ $# -eq 0 ] ; then 
	if [ $curdir == "/home/karthickraja" ] ; then 
		if [ $periodical -eq 1 ] ; then 
			$todo_sh -d $dummy_cfg -c list 
		else 
			$todo_sh list 
		fi 
	elif [ $localize -eq 1 ] ; then 
		$todo_sh -d $dummy_cfg -+ list 
	elif [ $periodical -eq 1 ] ; then 
		$todo_sh -d $dummy_cfg -c list $curdir 
	else 
		$todo_sh list $curdir 
	fi 
	exit 0 
fi 

if [[ $localize -eq 1 || $periodical -eq 1 ]] ; then 
#	echo $@ 
	$todo_sh -d $dummy_cfg $@ 
else 
	$todo_sh $@ 
fi 
