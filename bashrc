# se e/usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# All the PATH assignation has moved to .profile file. 
# PATH="/home/karthickraja/.bin/:$PATH;"

# HISTORY Related settings. 
	# don't put duplicate lines or lines starting with space in the history.
	# See bash(1) for more options
	HISTCONTROL=ignoreboth
	HISTIGNORE='l:pwd:ls'
	# append to the history file, don't overwrite it
	shopt -s histappend
	shopt -s histverify 	# To verify after getting command from history. 
	
	# to ignore the spell mistakes on the spell on cd command.
	shopt -s cdspell 
#	shopt -s dirspell 	# Commented due to the unsupported version of bash in appserver. 
	
	# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
	HISTSIZE=1000
	HISTFILESIZE=2000
	
	HISTCONTROL=ignoredups:erasedups:$HISTCONTROL # avoid duplicates..

	# After each command, save and reload history
#	PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history  -r;tail -2 ~/.bash_history >> ~/.my_hist" 
#	PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$'\n'}history -a; history -c; history -r;" 
	PROMPT_COMMAND="history -a; history -c; history -r" 
	HISTTIMEFORMAT="%H:%M:%S "

	#To get the previous entries one by one. 
	bind '"\e[A": history-search-backward'  
	bind '"\e[B": history-search-forward'  

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
export HOSTALIASES=~/.hosts

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes



if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
 #   PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
    PS1='karthi@\h on \@ :\w\$ '  # replace the last \# with  \$ for making dolor as prompt.
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
#    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    PS1="\[\e]0;Karthick Raja :\w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi


export USER_PASSWORD="protagonist" 

#For PSQL 
export PGHOST=192.168.1.6 
export CVSROOT=":pserver:karthickraja@192.168.1.11:2401/"

#For SSH alert 
if [ -n "$SSH_CLIENT" ] ; then 
	~/.bin/ssh_alert 
fi 

# For TODO command  
TODOTXT_DEFAULT_ACTION="list" 
#export TODOTXT_SORT_COMMAND='env LC_COLLATE=C sort -k 2,2 -k 1,1n'
if [ -f /home/karthickraja/Logs/Tasks/script/todo_completion ] ; then 
	. /home/karthickraja/Logs/Tasks/script/todo_completion 
fi 




export GEM_PATH='/home/karthickraja/.gem/ruby/2.3.0' 
export PATH="$PATH:/home/karthickraja/.gem/ruby/2.3.0/bin"

# Path to the bash it configuration
export BASH_IT="/home/karthickraja/.bash_it"

# Lock and Load a custom theme file
# location /.bash_it/themes/
export BASH_IT_THEME='bobby'

# (Advanced): Change this to the name of your remote repo if you
# cloned bash-it with a remote other than origin such as `bash-it`.
# export BASH_IT_REMOTE='bash-it'

# Your place for hosting Git repos. I use this for private repos.
export GIT_HOSTING='karthickraja@192.168.1.129'

# Don't check mail when opening terminal.
#unset MAILCHECK

# Change this to your console based IRC client of choice.
export IRC_CLIENT='irssi'

# Set this to the command you use for todo.txt-cli
export TODO="t"

# Set this to false to turn off version control status checking within the prompt for all themes
export SCM_CHECK=true

# Set vcprompt executable path for scm advance info in prompt (demula theme)
# https://github.com/djl/vcprompt
export VCPROMPT_EXECUTABLE=~/.vcprompt/bin/vcprompt

# (Advanced): Uncomment this to make Bash-it reload itself automatically
# after enabling or disabling aliases, plugins, and completions.
# export BASH_IT_AUTOMATIC_RELOAD_AFTER_CONFIG_CHANGE=1

# Load Bash It
source $BASH_IT/bash_it.sh


# Adding this for JABA 
export JMFHOME="/home/karthickraja/.lib/JMF-2.1.1e"
export LD_LIBRARY_PATH=$JMFHOME/lib
export CLASSPATH=$JMFHOME/lib/jmf.jar
export PATH="~/.lib/JMF-2.1.1e/bin/:$PATH"
export TZ="Asia/Kolkata"

## DEFAULT 
export PATH=$PATH:$HOME/.motivate
