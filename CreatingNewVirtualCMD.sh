#### VirtualBox Guest Creation from VMDK. 
 
#  * Installing the downloaded the extension pack into the virtualbox. 
#   `VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-4.3.36-105129a.vbox-extpack`

 # * Decide the configuration for the guest. 
 #  - Guest-Virtual-instance name. 
 instance_name="FMS-1.1"
   # Memory 
 ram_size="3072"
   # partition name
  partition_name=/dev/sda
  #  - partition no 
   partition_no=2
 #* Execute the below commands after setting up variables. 
  #```
  VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-4.3.36-105129a.vbox-extpack 
  VBoxManage createvm --name $instance_name --register
  VBoxManage modifyvm $instance_name --memory $ram_size --acpi on --boot1 disk
  VBoxManage modifyvm $instance_name --ostype Debian
  VBoxManage storagectl $instance_name --name "IDE Controller" --add ide
 	cd $HOME/VirtualBox\ Vms/$instance_name/
  VBoxManage internalcommands createrawvmdk -filename $instance_name.vmdk -rawdisk $partition_name -partitions $partition_no -relative
  VBoxManage storageattach $instance_name --storagectl "IDE Controller" --port 0 --device 0 --type hdd --medium ./$instance_name.vmdk
  vboxmanage hostonlyif create
  vboxmanage hostonlyif ipconfig vboxnet0 --ip 192.168.56.1
  VBoxManage modifyvm $instance_name --nic1 nat
  vboxmanage modifyvm $instance_name --nic2 hostonly --hostonlyadapter2 vboxnet0

  echo "Instance $instance_name created successfully."
  echo "Opening the instance in "
  nohup VBoxHeadless --startvm $instance_name --vrde on &
